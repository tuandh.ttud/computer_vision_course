import os
import shutil
import numpy as np
from os import listdir
from os.path import isfile, isdir, join
import cv2
import sys
from six.moves import cPickle

face_data_path = 'data_sample'
dest_data_path = 'lfitw'

cifar_data_batch_path = ''


# random pick 7k image from 13k image, prob pick = 7/13
# scale image from 250x250 to 32x32

def scale_and_save_image(source_image_file, des_image_file):
    source_img = cv2.imread(source_image_file)
    des_img = cv2.resize(source_img, (32, 32))
    return des_img


def load_face_image():
    all_folders = [f for f in listdir(face_data_path) if isdir(join(face_data_path, f))]
    for folder in all_folders:
        full_path_folder = join(face_data_path, folder)
        all_files = [f for f in listdir(full_path_folder) if isfile(join(full_path_folder, f))]
        count = 0
        for file in all_files:
            full_path_file = join(full_path_folder, file)
            full_path_file_valid = join(dest_data_path, file)
            if np.random.rand(1) < 0.54:
                scale_and_save_image(full_path_file, full_path_file_valid)
                count = count + 1
                if count >= 7000:
                    break
                if (count % 500) == 0:
                    print('saved ' + str(count) + ' image')


def load_cifar_image():
    return


def load_data():
    """Loads CIFAR10 dataset.

        # Returns
            Tuple of Numpy arrays: `(x_train, y_train), (x_test, y_test)`.
        """
    dirname = 'cifar-10-batches-py'
    origin = 'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz'
    path = get_file(dirname, origin=origin, untar=True)

    num_train_samples = 50000

    x_train = np.empty((num_train_samples, 3, 32, 32), dtype='uint8')
    y_train = np.empty((num_train_samples,), dtype='uint8')

    for i in range(1, 6):
        fpath = os.path.join(path, 'data_batch_' + str(i))
        (x_train[(i - 1) * 10000: i * 10000, :, :, :],
         y_train[(i - 1) * 10000: i * 10000]) = load_batch(fpath)

    fpath = os.path.join(path, 'test_batch')
    x_test, y_test = load_batch(fpath)

    y_train = np.reshape(y_train, (len(y_train), 1))
    y_test = np.reshape(y_test, (len(y_test), 1))

    if K.image_data_format() == 'channels_last':
        x_train = x_train.transpose(0, 2, 3, 1)
        x_test = x_test.transpose(0, 2, 3, 1)

    return (x_train, y_train), (x_test, y_test)


def load_batch(fpath, label_key='labels'):
    """Internal utility for parsing CIFAR data.

    # Arguments
        fpath: path the file to parse.
        label_key: key for label data in the retrieve
            dictionary.

    # Returns
        A tuple `(data, labels)`.
    """
    with open(fpath, 'rb') as f:
        if sys.version_info < (3,):
            d = cPickle.load(f)
        else:
            d = cPickle.load(f, encoding='bytes')
            # decode utf8
            d_decoded = {}
            for k, v in d.items():
                d_decoded[k.decode('utf8')] = v
            d = d_decoded
    data = d['data']
    labels = d[label_key]

    data = data.reshape(data.shape[0], 3, 32, 32)
    return data, labels
