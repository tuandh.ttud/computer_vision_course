import os
import shutil
import numpy as np
from os import listdir
from os.path import isfile, isdir, join
import cv2
import sys
from six.moves import cPickle
import keras.backend as K


face_data_path = 'data_sample'
dest_data_path = 'lfitw'


def scale_and_save_image(source_image_file, des_image_file):
    source_img = cv2.imread(source_image_file)
    des_img = cv2.resize(source_img, (32, 32))
    cv2.imwrite(des_image_file, des_img)


all_folders = [f for f in listdir(face_data_path) if isdir(join(face_data_path, f))]
for folder in all_folders:
    full_path_folder = join(face_data_path, folder)
    all_files = [f for f in listdir(full_path_folder) if isfile(join(full_path_folder, f))]
    for file in all_files:
        full_path_file = join(full_path_folder, file)
        full_path_file_valid = join(dest_data_path, file)
        scale_and_save_image(full_path_file, full_path_file_valid)
