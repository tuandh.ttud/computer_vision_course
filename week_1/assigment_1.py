import keras
from keras.datasets import cifar10
from os import listdir
from os.path import isfile, isdir, join
import cv2
import numpy as np
import random
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D

# Step 1: From ‘Labelled Faces in the Wild’’ dataset,
# randomly pick up 7,000 faces/images (5000 for training and 2000 for testing)
face_img_path = 'lfitw'


def load_face_data():
    data = []
    label = []

    # load and shuffle data
    img_files = [f for f in listdir(face_img_path) if isfile(join(face_img_path, f))]
    random.seed(123)
    random.shuffle(img_files)

    # split train and test data
    img_files = img_files[:7000]
    for img_file in img_files:
        img = cv2.imread(join(face_img_path, img_file))
        data.append(img)
        label.append([10])
    data, label = np.array(data), np.array(label)
    (x_train, x_test) = train_test_split(data, train_size=5000)
    return (x_train, label[:5000]), (x_test, label[5000:7000])


# Step 2: Combine the faces in Step 1 with CIFAR-10 to have CIFARE-11 dataset: 67000 images in 11 classes.
(face_x_train, face_y_train), (face_x_test, face_y_test) = load_face_data()
(cifar_x_train, cifar_y_train), (cifar_x_test, cifar_y_test) = cifar10.load_data()

x_train = np.concatenate((face_x_train, cifar_x_train), axis=0)
x_test = np.concatenate((face_x_test, cifar_x_test), axis=0)
y_train = np.concatenate((face_y_train, cifar_y_train), axis=0)
y_test = np.concatenate((face_y_test, cifar_y_test), axis=0)
y_train, y_test = to_categorical(y_train), to_categorical(y_test)


# Step 3: Build a CNN network to classify an image across 11 classes
def build_model(input_shape, num_classes):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same',
                     input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))
    return model


model = build_model(x_train.shape[1:], y_train.shape[0])
print()
